#!/usr/bin/env python3

# --------------
# TestCollatz.py
# --------------

# pylint: disable = bad-whitespace
# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import (
    collatz_read,
    max_cycle_length,
    cycle_length,
    collatz_eval,
    collatz_print,
    collatz_solve,
)


# -----------
# TestCollatz
# -----------


class TestCollatz(TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 1)
        self.assertEqual(j, 10)

    # ----
    # eval
    # ----
    def test_cycle_length_1(self):
        v = cycle_length(7)
        self.assertEqual(v, 17)

    def test_cycle_length_2(self):
        v = cycle_length(999999)
        self.assertEqual(v, 259)

    def test_cycle_length_3(self):
        v = cycle_length(1000000)
        self.assertEqual(v, 153)

    def test_cycle_length_4(self):
        v = cycle_length(15)
        self.assertEqual(v, 18)

    def test_cycle_length_5(self):
        v = cycle_length(1)
        self.assertEqual(v, 1)

    def test_max_cycle_length_1(self):
        v = max_cycle_length(1, 10, 15)
        self.assertEqual(v, 20)

    def test_max_cycle_length_2(self):
        v = max_cycle_length(1, 10, 30)
        self.assertEqual(v, 30)

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        v = collatz_eval(1000, 900)
        self.assertEqual(v, 174)

    def test_eval_6(self):
        v = collatz_eval(200, 2200)
        self.assertEqual(v, 182)

    def test_eval_7(self):
        v = collatz_eval(998999, 999999)
        self.assertEqual(v, 396)

    def test_eval_8(self):
        v = collatz_eval(500, 500)
        self.assertEqual(v, 111)

    def test_eval_9(self):
        v = collatz_eval(1, 500)
        self.assertEqual(v, 144)

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n"
        )


# ----
# main
# ----

if __name__ == "__main__":  # pragma: no cover
    main()
